// 1. bikin class Vehicle (Kendaraan)
// attribut nya (constructor) : jenis kendaraan (roda berapa 2/4 atau lainnya), negara produksi, 
class Kendaraan {
    constructor (jenisKendaraan, negaraProduksi){
        this.jenisKendaraan = jenisKendaraan;
        this.negaraProduksi = negaraProduksi;
    }
    // ada method info yang melakukan print = 'Jenis Kendaraan roda 4 dari negara Jerman';
    info(){
        console.log(`Jenis Kendaraan ${this.jenisKendaraan} dari negara ${this.negaraProduksi} `);
    }
}
 
// 2. bikin child class (Mobil) dari Kendaraan, inherit attribut jenis kendaraan dan negara produksi dari super/parent class nya
// attribut baru di child class ini yaitu Merek Kendaraan, Harga Kendaraan dan Persen Pajak
class Mobil extends Kendaraan {
    constructor (jenisKendaraan, negaraProduksi, merekKendaraan, hargaKendaraan, persenPajak){
        super (jenisKendaraan, negaraProduksi);
        this.merekKendaraan = merekKendaraan;
        this.hargaKendaraan = hargaKendaraan;
        this.persenPajak = persenPajak;
    }
    // 3. ada method totalPrice yang melakukan proses menambah harga normal dengan persen pajak, yang RETURN hasil penjumlahan tersebut
    totalHarga() {
        let hasil = this.hargaKendaraan + ((this.persenPajak / 100) * this.hargaKendaraan);
        return hasil;
    }
    // 4. overidding method info dari super/parent class (panggil instance method info dari super class = super.methodName() dan di overriding method info di child class ini ada tambahan =>
    // print = 'Kendaraan ini nama mereknya Mercedes dengan total harga Rp. 880.000.000'
    info() {
        super.info();
        console.log(`Kendaraan ini nama mereknya ${this.merekKendaraan} dengan total harga Rp. ${this.totalHarga()}`);
    }
}


// 5. buat 2 instance (1 dari parent class(Vehicle), 1 dari child class aja(Mobil)) 
const kendaraan = new Kendaraan(4, 'Jepang');
kendaraan.info();

const mobil = new Mobil (4, 'Jerman', 'Mercedes', 800000000, 10);
mobil.info();

// contoh instance
// 1. const kendaraan = new Vehicle(2, 'Jepang')
// kendaraan.info()
// 2. const mobil = new Mobil(4, 'Jerman', 'Mercedes', 800000000, 10);
// mobil.info()

/** 
    NOTES
    rumus total price setelah di tambah pajak utk method totalPrice, bisa kalian googling sendiri yah.
*/
